const express = require('express')
const router = express.Router()
const db = require('../db-connect')
const bodyParser = require('body-parser')
const urlencodedParser = bodyParser.json()

router.get('/', async function (req, res) {
    const r = await db.dbConnect().then((c) => {
        return c.find({}).toArray()
    });
    console.log('result: ', r);
    if (false) {
        res.status(400).json(
            {
                "message": "string"
            }
        )
    }
    res.status(200).json({
        "offset": 0,
        "limit": 0,
        "count": 0,
        "notes": r
    })
})

router.post('/', urlencodedParser, async function (req, res) {
    console.log(req.body, '345')
    const note = {
        userId: '5099803df3f4948bd2f98391',
        completed: false,
        text: req.body.text,
        createdDate: new Date().toISOString(),
    }
    const r = await db.dbConnect().then((c) => {
        return c.insertOne(note)
    });

    if (false) {
        res.status(400).json(
            {
                "message": "string"
            }
        )
    }
    res.status(200).json({
        "message": "Success"
    })
})

router.get('/:id', async function (req, res) {
    const r = await db.dbConnect().then((c) => {
        return c.find({}).toArray()
    });
    if (false) {
        res.status(400).json(
            {
                "message": "string"
            }
        )
    }
    res.status(200).json({
        "offset": 0,
        "limit": 0,
        "count": 0,
        "notes": r
    })
})

router.put('/:id', async function (req, res) {
    const r = await db.dbConnect().then((c) => {
        return c.find({}).toArray()
    });
    console.log('result: ', r);
    if (false) {
        res.status(400).json(
            {
                "message": "string"
            }
        )
    }
    res.status(200).json({
        "message": "Success"
    })
})

router.patch('/:id', async function (req, res) {
    const r = await db.dbConnect().then((c) => {
        return c.find({}).toArray()
    });
    console.log('result: ', r);
    if (false) {
        res.status(400).json(
            {
                "message": "string"
            }
        )
    }
    res.status(200).json({
        "message": "Success"
    })
})

router.delete('/:id', async function (req, res) {
    const r = await db.dbConnect().then((c) => {
        return c.find({}).toArray()
    });
    console.log('result: ', r);
    if (false) {
        res.status(400).json(
            {
                "message": "string"
            }
        )
    }
    res.status(200).json({
        "message": "Success"
    })
})

module.exports = router