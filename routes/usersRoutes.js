const express = require('express')
const router = express.Router()
const db = require('../db-connect')

router.get('/', async function (req, res) {
    const r = await db.dbConnect().then((c) => {
        return c.find({}).toArray()
    });
    console.log('result: ', r);
    if (false) {
        res.status(400).send(
            {
                "message": "string"
            }
        )
    }
    res.status(200).json({
        "user": {
            "_id": "5099803df3f4948bd2f98391",
            "username": "Kyrylo",
            "createdDate": "2020-10-28T08:03:19.814Z"
        }
    })
})

router.delete('/', async function (req, res) {
    const r = await db.dbConnect().then((c) => {
        return c.find({}).toArray()
    });
    console.log('result: ', r);
    if (false) {
        res.status(400).json(
            {
                "message": "string"
            }
        )
    }
    res.status(200).json({
        "message": "Success"
    })
})

router.patch('/', async function (req, res) {
    const r = await db.dbConnect().then((c) => {
        return c.find({}).toArray()
    });
    console.log('result: ', r);
    if (false) {
        res.status(400).json(
            {
                "message": "string"
            }
        )
    }
    res.status(200).json({
        "message": "Success"
    })
})

module.exports = router