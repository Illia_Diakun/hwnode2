const { MongoClient } = require('mongodb');

const url = 'mongodb+srv://Amelgal:Inter1688@cluster0.5royn.mongodb.net';
const client = new MongoClient(url);


const dbName = 'hw2';

async function dbConnect() {
    await client.connect();
    const db = client.db(dbName);
    const collection = db.collection('notes');
    return collection;
}

async function closeConnect() {
    return client.close()
}


    module.exports = {
        dbConnect, closeConnect, collection: dbConnect()
    };
