require('dotenv').config()
const express = require('express')
const app = express()
const port = process.env.PORT || 8080;
const db = require('./db-connect')

function logErrors (err, req, res, next) {
    console.error(err.stack)
    console.log('errorHandler')
    next(err)
}

app.use('/api/users', require('./routes/usersRoutes'))

app.use('/api/notes', require('./routes/notesRouter'))

app.use('/api/auth', require('./routes/authRouter'))

app.use(logErrors)

db.collection
    .then(() => {
        app.listen(port, async () => {
            console.log(`server started on port ${port}`)
            // await dbConnect()
            //     .then(console.log)
            //     .catch(console.error)
            //     .finally(() => client.close());
        });
    })


process.on('unhandledRejection', db.closeConnect);
process.on('uncaughtException', db.closeConnect);



